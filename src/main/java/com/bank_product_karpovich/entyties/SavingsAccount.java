package com.bank_product_karpovich.entyties;

import java.math.BigDecimal;

public class SavingsAccount extends AbstractBankProduct {
    public SavingsAccount(String currency, BigDecimal balance, String name) {
        super(currency, balance, name);
    }

    @Override
    public void deposit(double amount) {
    }

    @Override
    public void withdraw(double amount) {
    }

    @Override
    public BigDecimal checkBalance() {
        return getBalance();
    }

    public void closeAccount() {
        if (getBalance().compareTo(BigDecimal.ZERO) != 0) {
            throw new IllegalStateException("Cannot close account with a non-zero balance");
        }
    }
}

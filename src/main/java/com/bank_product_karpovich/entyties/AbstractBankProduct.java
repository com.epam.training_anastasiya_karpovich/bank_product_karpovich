package com.bank_product_karpovich.entyties;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public abstract class AbstractBankProduct implements BankingProduct {
    private String currency;
    private BigDecimal balance;
    private String name;

    public AbstractBankProduct(String currency, BigDecimal balance, String name) {
        this.currency = currency;
        this.balance = balance;
        this.name = name;
    }

    @Override
    public String toString() {
        return "AbstractBankProduct{" +
                "currency='" + currency + '\'' +
                ", balance=" + balance +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractBankProduct)) return false;
        AbstractBankProduct that = (AbstractBankProduct) o;
        return Objects.equals(getCurrency(), that.getCurrency()) && Objects.equals(getBalance(), that.getBalance()) && Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCurrency(), getBalance(), getName());
    }

    public void withdraw(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Amount must be greater than zero");
        }
        if (amount.compareTo(balance) <= 0) {
            balance = balance.subtract(amount);
        } else {
            throw new IllegalArgumentException("Insufficient funds");
        }
    }


    public void deposit(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Amount must be greater than zero");
        }
        balance = balance.add(amount);
    }

    public BigDecimal checkBalance() {
        return balance;
    }
}

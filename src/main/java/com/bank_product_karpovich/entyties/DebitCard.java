package com.bank_product_karpovich.entyties;

import java.math.BigDecimal;

public class DebitCard extends AbstractBankProduct {
    public DebitCard(String currency, BigDecimal balance, String name) {
        super(currency, balance, name);
    }

    @Override
    public void deposit(double amount) {
    }

    @Override
    public void withdraw(double amount) {
    }
}

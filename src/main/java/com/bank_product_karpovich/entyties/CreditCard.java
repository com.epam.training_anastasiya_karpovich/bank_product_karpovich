package com.bank_product_karpovich.entyties;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public class CreditCard extends AbstractBankProduct {
    private BigDecimal interestRate;

    public CreditCard(String currency, BigDecimal balance, String name, BigDecimal interestRate) {
        super(currency, balance, name);
        this.interestRate = interestRate;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "interestRate=" + interestRate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditCard)) return false;
        if (!super.equals(o)) return false;
        CreditCard that = (CreditCard) o;
        return Objects.equals(getInterestRate(), that.getInterestRate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getInterestRate());
    }

    @Override
    public void deposit(double amount) {
    }

    @Override
    public void withdraw(double amount) {
    }

    public BigDecimal checkInterestRate() {
        return interestRate;
    }
}

package com.bank_product_karpovich.entyties;

import java.math.BigDecimal;

interface BankingProduct {
    String getCurrency();

    String getName();

    void deposit(double amount);

    void withdraw(double amount);

    BigDecimal checkBalance();
}
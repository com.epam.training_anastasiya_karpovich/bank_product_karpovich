package com.bank_product_karpovich.unitTest;

import com.bank_product_karpovich.entyties.CreditCard;
import com.bank_product_karpovich.entyties.DebitCard;
import com.bank_product_karpovich.entyties.SavingsAccount;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankProductTest {

    @Test
    public void debitCardDepositTest() {
        DebitCard debitCard = new DebitCard("USD", BigDecimal.valueOf(1000.00), "Debit Card");
        debitCard.deposit(BigDecimal.valueOf(500.00));
        assertEquals(BigDecimal.valueOf(1500.00), debitCard.getBalance());
    }

    @Test
    public void creditCardWithdrawTest() {
        CreditCard creditCard = new CreditCard("USD", BigDecimal.valueOf(1000.0), "Credit Card", BigDecimal.valueOf(0.05));
        creditCard.withdraw(BigDecimal.valueOf(500.0));
        assertEquals(BigDecimal.valueOf(500.0), creditCard.getBalance());
    }

    @Test
    public void savingsAccountCloseTest() {
        SavingsAccount savingsAccount = new SavingsAccount("EUR", BigDecimal.valueOf(0.0), "Savings Account");
        savingsAccount.closeAccount();
        assertEquals(BigDecimal.valueOf(0.0), savingsAccount.getBalance());
    }
}